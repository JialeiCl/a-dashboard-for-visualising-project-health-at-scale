# A Dashboard for visualising project health at scale

Run the project:

~$ python manage.py makemigrations dashboard

~$ python manage.py migrate

~$ python population_script.py

~$ python manage.py createsuperuser

~$ python manage.py runserver

